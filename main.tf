terraform { #terraform block
  required_providers {
    aws = {
      #source = "hashicorp/aws"
      #version = "5.5.0" #not compulsory unless you want a specific version 
    }
  }
}

provider "aws" { #compulsory you need it if not it won't work/#provider block
  #access_key = "" you can't use it it's not secure
  #secret_key = "" use aws configure....Then you need to initalize your terraform provider....terraform init command
  region = "us-east-1" #Good practice don't hardcode your credentials-aws configure

  # Configuration options
}
resource "aws_vpc" "main" { # "name of resource aws_vpc/fixed telling aws to create a vpc and 
  #main is the referece name-the name not the vpc name" -used for reference/ like a logical ID
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "terraform-vpc"
  }
}
resource "aws_subnet" "subnet-1" {
  vpc_id            = aws_vpc.main.id # ref logical id creating from the previous vpc.id to get id only
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a" #optional use it when you want to 
  tags = {
    Name = "terraform-subnet-1"
  }
}

data "aws_vpc" "default_vpc" {
  default = true #bolean true get the default vpc false will not get it from the default vpc
}
resource "aws_subnet" "subnet-2" {
  vpc_id            = data.aws_vpc.default_vpc.id # use the data and the name of data source and the name of logical id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b" #optional use it when you want to 
  tags = {
    Name = "terraform-subnet-2"

  }
}

